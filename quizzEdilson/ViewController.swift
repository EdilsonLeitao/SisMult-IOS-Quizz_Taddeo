//
//  ViewController.swift
//  quizzEdilson
//
//  Created by Ios on 06/04/18.
//  Copyright © 2018 br.edu.uni7. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var questions:[Question]!
    
    var qidAtual = 0
    
    @IBOutlet weak var uiImage: UIImageView!
    @IBOutlet weak var lbPergunta: UILabel!
    @IBOutlet weak var btResposta1: UIButton!
    @IBOutlet weak var btResposta2: UIButton!
    @IBOutlet weak var btResposta3: UIButton!
    @IBOutlet weak var btResposta4: UIButton!
    @IBOutlet weak var lbResultado: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let q0a1 = Answer( answer:"116 anos", isCorrect:true )
        let q0a2 = Answer( answer:"120 anos", isCorrect:false )
        let q0a3 = Answer( answer:"100 anos", isCorrect:false )
        let q0a4 = Answer( answer:"80 anos", isCorrect:false )
        let q0 = Question( question:"Quanto tempo durou a guerra dos 100 anos?",
                           strImageFileName: "guerra.jpg", answers:[ q0a1, q0a2, q0a3, q0a4 ] )
        
        let q1a1 = Answer( answer:"Brasil", isCorrect:false )
        let q1a2 = Answer( answer:"Chile", isCorrect:false )
        let q1a3 = Answer( answer:"Panamá", isCorrect:false )
        let q1a4 = Answer( answer:"Equador", isCorrect:true )
        let q1 = Question( question:"Em que país é fabricado o chapéu Panamá?",
                           strImageFileName: "chapeu.jpg", answers:[ q1a1, q1a2, q1a3, q1a4 ] )

        let q2a1 = Answer( answer:"Janeiro", isCorrect:false )
        let q2a2 = Answer( answer:"Setembro", isCorrect:false )
        let q2a3 = Answer( answer:"Outubro", isCorrect:false )
        let q2a4 = Answer( answer:"novembro", isCorrect:true )
        let q2 = Question( question:"Em que mês os Russos celebram a revolução de outubro?",
                           strImageFileName: "revolucao.jpg", answers:[ q2a1, q2a2, q2a3, q2a4 ] )

        let q3a1 = Answer( answer:"Éder", isCorrect:false )
        let q3a2 = Answer( answer:"Albert", isCorrect:true )
        let q3a3 = Answer( answer:"George", isCorrect:false )
        let q3a4 = Answer( answer:"Manoel", isCorrect:false )
        let q3 = Question( question:"Qual era o primeiro nome do Rei George VI?",
                           strImageFileName: "rei.jpg", answers:[ q3a1, q3a2, q3a3, q3a4 ] )

        let q4a1 = Answer( answer:"Canário", isCorrect:false )
        let q4a2 = Answer( answer:"Uurubu", isCorrect:false )
        let q4a3 = Answer( answer:"Cachorro", isCorrect:true )
        let q4a4 = Answer( answer:"Rato", isCorrect:false )
        let q4 = Question( question:"As ilhas Canárias no oceano Atlântico tem seu nome tirado de que animal?",
                           strImageFileName: "canarias.png", answers:[ q4a1, q4a2, q4a3, q4a4 ] )
        
        let q5a1 = Answer( answer:"25", isCorrect:false )
        let q5a2 = Answer( answer:"30", isCorrect:true )
        let q5a3 = Answer( answer:"31", isCorrect:false )
        let q5a4 = Answer( answer:"29", isCorrect:false )
        let q5 = Question( question:"Quanto tempo durou a guerra dos 30 anos?",
                           strImageFileName: "guerra30.jpg", answers:[ q5a1, q5a2, q5a3, q5a4 ] )
        
        let q6a1 = Answer( answer:"KD", isCorrect:false )
        let q6a2 = Answer( answer:"KM", isCorrect:true )
        let q6a3 = Answer( answer:"KK", isCorrect:false )
        let q6a4 = Answer( answer:"KG", isCorrect:false )
        let q6 = Question( question:"Qual desses símbolos significa Quilometro?",
                           strImageFileName: "6.jpg", answers:[ q6a1, q6a2, q6a3, q6a4 ] )

        let q7a1 = Answer( answer:"Lenin", isCorrect:false )
        let q7a2 = Answer( answer:"Gorbatchov", isCorrect:false )
        let q7a3 = Answer( answer:"Karl Marx", isCorrect:true )
        let q7a4 = Answer( answer:"Allan Kardec", isCorrect:false )
        let q7 = Question( question:"Quem é o autor do manifesto comunista?",
                           strImageFileName: "7.jpg", answers:[ q7a1, q7a2, q7a3, q7a4 ] )
        
        let q8a1 = Answer( answer:"Jegue", isCorrect:false )
        let q8a2 = Answer( answer:"Cavalo", isCorrect:false )
        let q8a3 = Answer( answer:"Touro", isCorrect:true )
        let q8a4 = Answer( answer:"Hipopótamo", isCorrect:false )
        let q8 = Question( question:"Qual o animal que representa o signo de touro?",
                           strImageFileName: "8.png", answers:[ q8a1, q8a2, q8a3, q8a4 ] )
        
        let q9a1 = Answer( answer:"Astigmatismo", isCorrect:false )
        let q9a2 = Answer( answer:"Pedofilia", isCorrect:false )
        let q9a3 = Answer( answer:"Vitiligo", isCorrect:true )
        let q9a4 = Answer( answer:"Bruxismo", isCorrect:false )
        let q9 = Question( question:"Como é chamada a doença que está clareando a pele do Michael Jackson?",
                           strImageFileName: "9.jpg", answers:[ q9a1, q9a2, q9a3, q9a4 ] )
        
        questions = [ q0, q1, q2, q3, q4, q5, q6, q7, q8, q9 ]
        qidAtual = 0
        ShowQuestion(qid: qidAtual)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func ShowQuestion( qid:Int ){
        lbPergunta.text = questions[ qid ].strQuestion
        uiImage.image = questions[ qid ].imgQuestion
        btResposta1.setTitle( questions[ qid ].answers[ 0 ].strAnswer, for: .normal )
        btResposta2.setTitle( questions[ qid ].answers[ 1 ].strAnswer, for: .normal )
        btResposta3.setTitle( questions[ qid ].answers[ 2 ].strAnswer, for: .normal )
        btResposta4.setTitle( questions[ qid ].answers[ 3 ].strAnswer, for: .normal )
    }
    
    func validaResposta( answer:Answer ){
        if( answer.isCorrect == false ){
            lbResultado.text = "A resposta está errada!"
        } else {
            lbResultado.text = "A resposta está correta!"
            if( questions.count > ( qidAtual + 1 ) ){
                qidAtual += 1
                ShowQuestion(qid: qidAtual)
            } else {
                //TODO: Finalizar teste
            }
        }
    }
    
    @IBAction func brResposta1Action(_ sender: Any ) {
        validaResposta(answer: questions[ qidAtual ].answers[ 0 ] )
    }
    
    @IBAction func brResposta2Action(_ sender: Any) {
        validaResposta(answer: questions[ qidAtual ].answers[ 1 ] )
    }
    
    @IBAction func brResposta3Action(_ sender: Any) {
        validaResposta(answer: questions[ qidAtual ].answers[ 2 ] )
    }
    
    @IBAction func brResposta4Action(_ sender: Any) {
        validaResposta(answer: questions[ qidAtual ].answers[ 3 ] )
    }
}

